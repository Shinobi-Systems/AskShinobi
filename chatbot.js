const express = require('express');
const bodyParser = require('body-parser');
const app = express()
const http = require("http");
const Discord = require("discord.js");
const askShinobi = new Discord.Client();
const webhookBot = new Discord.Client();
var config=require('./conf.json')

s={}
s.gid=function(x){
    if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < x; i++ )
        t += p.charAt(Math.floor(Math.random() * p.length));
    return t;
};
if(!config.port)config.port = 8044
if(!config.webhookKey){
    config.webhookKey = s.gid()
    console.log('No Key created, random generated : '+config.webhookKey)
}
s.oneTimeMessages={}
webhookBot.on('ready', () => {
    console.log(`Logged in as ${webhookBot.user.tag}!`);
});
askShinobi.on('ready', () => {
    console.log(`Logged in as ${askShinobi.user.tag}!`);
    s.askShinobiTag=askShinobi.user.tag
});
askShinobi.on('message', msg => {
    msg.username=msg.author.username+'#'+msg.author.discriminator
    if(msg.username===s.askShinobiTag){return}
    var foundTerm;
    msg.search=function(x,y){
        foundTerm=x;
        var content = msg.content.toLowerCase().indexOf(x)
        if(y==true){
            y=content
        }else{
            y=content>-1
        }
        return y
    }
    switch(true){
        case msg.search('shinobi on mobile'):case msg.search('shinobi on the go'):case msg.search('shinobi mobile'):
            msg.theReply='This is what you probably want http://shinobi.video/articles/2017-08-03-shinobi-on-the-go'
        break;
        case msg.search('/search ')&&msg.search('/search',true)===0:
            msg.content=msg.content.replace('/search ','').trim()
            if(msg.content==''){
                return
            }
            http.get('http://shinobi.video/articles/search?search='+msg.content, function(data) {
                data.setEncoding('utf8');
                var chunks='';
                data.on('data', (chunk) => {
                    chunks+=chunk;
                });
                data.on('end', () => {
                    try{
                        chunks=JSON.parse(chunks)
                        if(chunks.ok===true&&chunks.articles.length>0){
                            msg.theReply='Here try one of these links :\n'
                            chunks.articles.forEach(function(v){
                                msg.theReply+='http://shinobi.video/articles/'+v.id+'\n'
                            })
                        }
                    }catch(er){
                        console.log(er)
                    }
                    if(!msg.theReply){
                        msg.theReply='Sorry there aren\'t results for your search'
                    }
                    msg.reply(msg.theReply);
                });

            }).on('error', function(e) {

            }).end();
        break;
    }
    if(msg.theReply){
        msg.reply(msg.theReply);
    }
});
webhookBot.login(config.loginToken);
askShinobi.login(config.chatLoginToken);


//Web stuff
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json('application/json'));

app.get('/', function (req, res) {
    res.send({ok:true,msg:'Webhook On'});
})
app.all('/webhook/:key', function (req, res) {
    if(req.params.key !== config.webhookKey){
        res.send({ok:false,msg:'Authentication Failed'});
        return
    }
    var form = req.body
    if(!form || !form.repository){
        res.send({ok:false,msg:'Empty Webhook',body:form});
        return console.log('Empty Webhook')
    }
    var commits = form.commits
    var branch = form.ref.split('/')
    branch = branch[branch.length - 1]
    var sendBody = {
        color: 3447003,
        author: {
          name: form.user_username,
          icon_url: form.user_avatar
        },
        title: '['+form.repository.name+':'+branch+'] '+form.total_commits_count+' new commits',
        url: "https://gitlab.com/Shinobi-Systems/Shinobi/compare/"+commits[commits.length - 1].id+"..."+commits[0].id,
        description: "",
        fields: [],
        timestamp: new Date(),
        footer: {
          icon_url: "https://shinobi.video/libs/assets/icon/apple-touch-icon-152x152.png",
          text: "Shinobi Systems"
        }
      }
    commits.forEach(function(commit){
        var filesText = []
        if(commit.added.length > 0)filesText.push(commit.added.length+' Files Added')
        if(commit.modified.length > 0)filesText.push(commit.modified.length+' Files Modified')
        if(commit.removed.length > 0)filesText.push(commit.removed.length+' Files Removed')
        sendBody.fields.push({
            name:commit.message,
            value: '['+commit.id.substring(0,8)+']('+commit.url+') `'+filesText.join(', ')+'`'
        })
    })
    webhookBot.channels.get(config.channel).send({embed: sendBody})
  res.send({message:"we received webhook"});
})

app.listen(config.port);